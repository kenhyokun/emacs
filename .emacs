;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

;; NOTE: (something important but I always forgot)
;; semantic-mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -- bulk rename/batch rename --     ;;
;; M-x dired			      ;;
;; M-x dired-toggle-read-only C-x C-q ;;
;; do some edit			      ;;
;; M-x wdired-finish-edit C-c C-c     ;;
;; -- bulk rename/batch rename --     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)

;;use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;;counsel & swiper
(use-package counsel
  :ensure t
  )

;; window 
(add-to-list 'initial-frame-alist '(fullscreen . maximized))
(add-to-list 'default-frame-alist '(fullscreen . fullheight))
(split-window-horizontally)

;; (require 'auto-complete)
;; (ac-config-default)
;; (global-auto-complete-mode t)

;;dired increment numeric sort
(setq dired-listing-switches "-laGh1v --group-directories-first")

;;bar
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode 0)
;;(horizontal-scroll-bar-mode t)

;;cursor
(setq-default cursor-type 'bar)
(set-cursor-color "#ffffff")
(setq default-frame-alist'((cursor-color . "green")))
(global-hl-line-mode 1)
;;(set-face-background 'hl-line "#ffffff")
;;(set-face-foreground 'highlight nil)

;;turncate lines
(set-default 'truncate-lines t)
(setq truncate-partial-width-windows nil)
(add-hook 'diff-mode-hook (lambda () (setq truncate-lines t)))

;; replace when yanking
(delete-selection-mode t)

;;switch split mode
(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))


;; evil mode
(add-to-list 'load-path "~/.emacs.d/evil")
(require 'evil)
(evil-mode 1)

;; linum
(add-hook 'find-file-hook 'linum-relative-mode)

;;keybindings
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "C-x b") 'counsel-switch-buffer)
(global-set-key (kbd "C-x C-b") 'counsel-switch-buffer-other-window)
(global-set-key (kbd "C-x g") 'shell)
(global-set-key (kbd "C-}") 'enlarge-window-horizontally)
(global-set-key (kbd "C-{") 'shrink-window-horizontally)
(global-set-key (kbd "C->") 'enlarge-window)
(global-set-key (kbd "C-<") 'shrink-window)
;; (global-set-key (kbd "M-r") 'replace-rectangle)
;; (global-set-key (kbd "M-R") 'delete-rectangle)
;; (global-set-key (kbd "M-w") 'replace-string)
;; (global-set-key (kbd "M-n") 'occur)
;; (global-set-key (kbd "M-N") 'grep)
;; (global-set-key (kbd "M-g") 'goto-line)
;; (global-set-key (kbd "M-o") 'query-replace)
;; (global-set-key (kbd "C-c C-c") 'comment-region)
;; (global-set-key (kbd "C-a") 'mark-page)
(define-key global-map [f9] 'evil-mode)

;;disabpe auto save and backup file
(setq backup-inhibited t)
(setq auto-save-mode 'f)
(setq make-backup-files nil)

;;smooth scroll
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))    
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)
(setq scroll-step 1)

;;custom color scheme
(add-to-list 'default-frame-alist '(font . "Liberation Mono-11.5"))
(set-face-attribute 'default t :font "Liberation Mono-11.5")
(set-face-attribute 'font-lock-builtin-face nil :foreground "#DAB98F")
(set-face-attribute 'font-lock-comment-face nil :foreground "gray50")
(set-face-attribute 'font-lock-constant-face nil :foreground "olive drab")
(set-face-attribute 'font-lock-doc-face nil :foreground "gray50")
(set-face-attribute 'font-lock-function-name-face nil :foreground "DarkGoldenrod3")
(set-face-attribute 'font-lock-keyword-face nil :foreground "DarkGoldenrod3")
(set-face-attribute 'font-lock-string-face nil :foreground "olive drab")
(set-face-attribute 'font-lock-type-face nil :foreground "brown3") ;; typename
(set-face-attribute 'font-lock-variable-name-face nil :foreground "sienna3")
(set-foreground-color "SkyBlue3") ;; burlywood3
(set-background-color "#161616")
(set-cursor-color "#40FF40")
(set-face-background 'hl-line "midnight blue") ;;midnight blue


(custom-set-variables

 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-safe-themes
   (quote
    ("e61752b5a3af12be08e99d076aedadd76052137560b7e684a8be2f8d2958edc3" "13d20048c12826c7ea636fbe513d6f24c0d43709a761052adbca052708798ce3" default)))
 '(package-selected-packages
   (quote
    (ace-window php-mode js2-mode linum-relative key-chord csharp-mode moe-theme counsel swiper use-package yasnippet company-irony company-irony-c-headers irony undo-tree powerline omnisharp nlinum-relative goto-chg company ac-php ac-c-headers)))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 120 :width normal)))))
